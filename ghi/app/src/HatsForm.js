import React, { useState, useEffect } from 'react';

function HatsForm() {
    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style_name, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPicture] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    // const [location, setMaxPresentaions] = useState('');
    // const [location, setLocation] = useState([]);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleFabric = (event) => {
        const value = event.target.value;
        setFabric(value);
    };

    const handleStyle = (event) => {
        const value = event.target.value;
        setStyle(value);
    };

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const handlePicture = (event) => {
        const value = event.target.value;
        setPicture(value);
    };


    const handleLocation = (event) => {
        const value = event.target.value;
        setLocation(value);
    };


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.fabric = fabric;
        data.style_name = style_name;
        data.color = color;
        data.picture_url = picture_url;
        data.location = location;

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newHats = await response.json();
            console.log(newHats);

            setName('');
            setFabric('');
            setStyle('');
            setColor('');
            setPicture('');
            setLocation('');
            window.location.replace('http://localhost:3000/hats')
        }
    };



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        // console.log(response)
        if (response.ok) {
          const data = await response.json();
          // console.log(data)
          setLocations(data.locations);
        }
      }
      // console.log(locations)
      useEffect(() => {
        fetchData();
      },[]);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create new Hats</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabric} value={fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyle} value={style_name} placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColor} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="picture_url">Picture Url</label>
                <input onChange={handlePicture} value={picture_url} placeholder="Picture Url" rows="3" className="form-control" required type="text" id="picture_url"  name="picture_url"></input>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleLocation} value={location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.href} value={location.href}>
                        {location.closet_name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
   );
}

export default HatsForm;
