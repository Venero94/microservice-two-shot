
import {Link} from "react-router-dom"


function HatsList(props) {
    return (
        <>
        <Link to='/hats/new'>Make a new Hat</Link>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Name</th>
                <th>Fabric</th>
                <th>Style</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.hats?.map(hat => {
                return (
                    <tr key={hat.id}>
                    <td><a href={hat.picture_url}>
                        <img src={hat.picture_url} className="img-thumbnail" alt="Thumbnail" width="50px" height="50px"></img>
                    </a></td>
                    <td>{ hat.name }</td>
                    <td>{ hat.fabric}</td>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.location.closet_name}</td>
                    <td>{ hat.location.shelf_number}</td>
                    <td>
                        <button onClick={() => {
                            const url=`http://localhost:8090/api/hats/${hat.id}`;
                            fetch(url, {method:"DELETE"}).then(response => {
                                console.log(response.status)
                                document.location.reload()
                            })
                        }} className="btn btn-primary">DELETE</button>
                    </td>
                    </tr>
                );
                })}
            </tbody>
        </table>
        </>
    );
}

export default HatsList;
