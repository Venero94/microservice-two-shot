import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsForm from './HatsForm';
import HatsList from './HatsList';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" >
            <Route path="new" element={<HatsForm />} />
            <Route path="" element={<HatsList hats={props.hats} />} />
          </Route>
          <Route path="shoes">
            <Route path="new" element={<ShoeForm/>}/>
            <Route path="" element={<ShoeList shoes={props.shoes}/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
