import React, {useEffect, useState} from "react";

function ShoeForm (){
  const [manufacturer, setManufacturer]=useState('')
  const [modelName, setModelName]=useState('')
  const [color, setColor]=useState('')
  const [pictureUrl, setPictureUrl]=useState('')
  const [bin, setBin]=useState('')
  const [bins, setBins]=useState([])

  const handleManChange=(event)=>{
    const value=event.target.value;
    setManufacturer(value)
  }

  const handleNameChange=(event)=>{
    const value = event.target.value;
    setModelName(value)
  }

  const handleColorChange=(event)=>{
    const value =event.target.value
    setColor(value)
  }

  const handlePictureChange=(event)=>{
    const value= event.target.value
    setPictureUrl(value)
  }

  const handleBinChange=(event)=>{
    const value=event.target.value
    setBin(value)
  }

  const handleSubmit=async(event)=>{
    event.preventDefault()
    const data={}
    data.manufacturer=manufacturer
    data.model_name=modelName
    data.color=color
    data.picture_url=pictureUrl
    data.bin=bin
    console.log(data)

    const url='http://localhost:8080/api/shoes/';

    const fetchConfig={
      method:"post",
      body: JSON.stringify(data),
      headers:{
        'Content-Type':'application/json',
      }
    }

    const response= await fetch(url, fetchConfig);
    if (response.ok){
      const newShoe=await response.json()
      console.log(newShoe)
      setManufacturer('')
      setModelName('')
      setColor('')
      setPictureUrl('')
      setBin('')
      window.location.replace('http://localhost:3000/shoes')
    }
  }

  const fetchData=async()=>{
    const url='http://localhost:8100/api/bins/';
    const response=await fetch(url)
    if (response.ok){
      const data=await response.json()
      setBins(data)
    }
  }

  useEffect(()=>{
    fetchData();
  }, [])


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Shoe!</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleManChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={modelName}  placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color}  placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={pictureUrl}  placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} value={bin}  required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.bins?.map(bin=>{
                    return (
                      <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default ShoeForm
