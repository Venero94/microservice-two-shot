from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVOEncoder(ModelEncoder):
    model=BinVO
    properties=[
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href"
    ]

class ShoeEncoder(ModelEncoder):
    model= Shoe
    properties=[
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders= {
        "bin":BinVOEncoder(),
    }


@require_http_methods(["GET","POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method=="GET":
        if bin_vo_id is not None:
            shoes=Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes= Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            bin_href=content["bin"]
            bin=BinVO.objects.get(import_href=bin_href)
            content["bin"]=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid Bin ID"},
                status=400,
            )
        shoe=Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    if request.method=="GET":
        try:
            shoe= Shoe.objects.get(id=id)
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response=JsonResponse({"message": "Shoe does not exist"})
            response.status_code=404
            return response
    elif request.method=="DELETE":
        try:
            count,_=Shoe.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count>0})
        except Shoe.DoesNotExist:
            response=JsonResponse({"message": "SHoe does not exist"})
    else:
        try:
            content=json.loads(request.body)
            shoe=Shoe.objects.get(id=id)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=400
            )
        Shoe.objects.filter(id=id).update(**content)
        shoes=Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeEncoder,
            safe=False
        )
# Create your views here    .
