from django.urls import path
from .views import api_list_hat, api_detail_hat


urlpatterns = [
    path("locations/<int:location_vo_id>/hats/", api_list_hat, name="api_list_hat"),
    path("hats/<int:id>/", api_detail_hat, name="api_detail_hat"),
    path("hats/", api_list_hat, name="api_create_hat"),
]
