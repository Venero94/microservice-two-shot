# Generated by Django 4.0.3 on 2023-04-19 23:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Hats',
            new_name='Hat',
        ),
    ]
